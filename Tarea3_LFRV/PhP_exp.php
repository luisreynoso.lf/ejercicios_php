


<?php
//Realizar una expresión regular que detecte emails correctos.
//$cadena = "Luis@GMAILcom"; CADENA ERRONEA
$cadena = 'luis@gmail.com'; //CADENA CORRECTA
$result = preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/", $cadena);
echo "Email correcto: ", $result; 
?>

</br>
<?php
//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
//$cadena = 'ADSAASD24342SAD1234'; CADENA ERRONEA
$cadena = 'REVL990315HDFYZS07'; //CADENA CORRECTA
$result = preg_match("/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/", $cadena);
echo "Curp correcto: ",$result; 


?>
</br>
<?php
//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
//$cadena = 'AAAAAAAAAAAA' CADENA INCORRECTA PUES ES MENOR A 50
$cadena = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'; //CADENA CORRECTA
$result = preg_match("/^[a-zA-Z]{50,}$/", $cadena);
echo "Cadena correcta: ",$result; 

?>
</br>
<?php
//Crea una funcion para escapar los simbolos especiales.
$str = "quita los simbolos especiales : ' \*#%!'";
echo addslashes($str);
?>
</br>
<?php
//Crear una expresion regular para detectar números decimales.
//$cadena = 'asdfg'; CADENA INCORRECTA
$cadena = '123.123'; //CADENA CORRECTA PARA CANTIDAD ILIMITADA DE DECIMALES 
$result = preg_match("/^-?(([1-9]\d*)|0)(.0*[1-9](0*[1-9])*)?$/", $cadena);
echo "Numeros decimales: ", $result; 


?>
</br>

